﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UoWDemo.Core;

namespace UoWDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            var unitOfWorkFactory = new UnitOfWorkFactory();

            var People = new List<Person>
            {
                new Person{Name = "Adam",Age = 21},
                new Person{Name = "Bogdan",Age =23}
            };

            var unitOfWork = unitOfWorkFactory.Create();

            People.Where(x => x.Name == "Bogdan").FirstOrDefault().Age = 10;
            People.Add(new Person { Age = 199, Name = "AAAA" });

            try
            {
                unitOfWork.Rollback();
            }
            catch (Exception ex)
            {
                unitOfWork.Rollback();
            }
        }
    }
}
