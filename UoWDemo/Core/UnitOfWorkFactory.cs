﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UoWDemo.Core
{
    public class UnitOfWorkFactory
    {
        public UnitOfWork Create()
        {
            return new UnitOfWork();
        }
    }
}
