﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UoWDemo.Core
{
    public interface IUnitOfWork
    {
        void Create();
        void Commit();
        void Rollback();
    }

    public class UnitOfWork : IUnitOfWork
    {
        public void Commit()
        {
            // prubujemy zapisac zmiany do bazy
        }

        public void Create()
        {
            //Punkt statrowy naszego unit of wok 
        }

        public void Rollback()
        {
            // cofni wszystkie zmiany
        }
    }
}
